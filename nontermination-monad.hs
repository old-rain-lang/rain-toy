import Control.Monad

data Result s r a = Success s a r | Failure s r

data Computation s r a = Computation (s -> r -> Result s r a)

instance Functor (Computation s c) where
    fmap f c = c >>= (return . f)

instance Applicative (Computation s c) where
  pure = return
  (<*>) = ap

instance Monad (Computation s c) where
  return affine = Computation (\state -> \relevant -> Success state affine relevant)
  (Computation c) >>= f =
    Computation
      ( \state -> \relevant -> case c state relevant of
          Success state affine relevant -> case f affine of
              Computation c -> c state relevant 
          Failure state relevant -> Failure state relevant
      )